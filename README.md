Unlocks the Teladi game start for the game "X4: Foundations", in case you
want/have to play without always-on internet.



This mainly concerns users of the [GOG](https://www.gog.com/game/x4_foundations)
Linux version, because even if you don't care about achievements, they are
required to unlock some aspects of the game.  
Whether you miss it or not, there is no GOG Galaxy for Linux, which is
apparently required to "validate" your "achievements" (did I hear someone there
in the back row whisper "DRM"?), so this is the way to go.



The modification itself is trivial, but it **changes your game to "modified"**,
so you can't use the _Ventures_ "multiplayer" component of the game, and you
are no longer eligible for _official_ support from Egosoft in case of any
technical problems.